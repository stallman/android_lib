package com.atguigu.android.spanimage;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.style.ImageSpan;
import android.widget.TextView;

import com.atguigu.android.BaseActivity;

public class TestSpan extends BaseActivity {

    private TextView testTxt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initView() {
        ImageSpan imageSpan = new ImageSpan(mContext, Uri.parse("http://img.huskybbbb.com/ffb6745b642a4fa46ef5bd57ef8af38a.jpeg"));
    }

    @Override
    protected void initData() {

    }


}
