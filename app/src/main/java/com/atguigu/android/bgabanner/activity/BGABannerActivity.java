package com.atguigu.android.bgabanner.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.atguigu.android.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.Arrays;

import cn.bingoogolapple.bgabanner.BGABanner;
import cn.bingoogolapple.bgabanner.BGALocalImageSize;

public class BGABannerActivity extends Activity {

    private Context mContext;
    private BGABanner mContentBanner;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;

        setContentView(R.layout.activity_bga_banner);

        mContentBanner = findViewById(R.id.bgaBanner);

        // 监听广告item的单击事件，在BGABanner里已经处理了防止重复点击事件
        mContentBanner.setDelegate(new BGABanner.Delegate() {
            @Override
            public void onBannerItemClick(BGABanner banner, View itemView, @Nullable Object model, int position) {
                Toast.makeText(banner.getContext(), "点击了第" + (position + 1) + "页", Toast.LENGTH_SHORT).show();
            }
        });

        // 配置数据源的方式1：通过传入数据模型并结合 Adapter 的方式配置数据源。这种方式主要用于加载网络图片，以及实现少于3页时的无限轮播
        mContentBanner.setAdapter(new BGABanner.Adapter<ImageView, String>() {
            @Override
            public void fillBannerItem(BGABanner banner, ImageView itemView, String model, int position) {
                Glide.with(mContext)
                        .load(model).apply(new RequestOptions().placeholder(R.mipmap.holder).error(R.mipmap.holder).centerCrop().dontAnimate())
                        .into(itemView);
            }
        });
        mContentBanner.setData(Arrays.asList("网络图片路径1", "网络图片路径2", "网络图片路径3"), Arrays.asList("提示文字1", "提示文字2", "提示文字3"));

        // 配置数据源的方式2：通过直接传入视图集合的方式配置数据源，主要用于自定义引导页每个页面布局的情况
//        List<View> views = new ArrayList<>();
//        views.add(View.inflate(context, R.layout.layout_guide_one, null));
//        views.add(View.inflate(context, R.layout.layout_guide_two, null));
//        views.add(View.inflate(context, R.layout.layout_guide_three, null));
//        mContentBanner.setData(views);

        // 配置数据源的方式3：通过传入图片资源 id 的方式配置数据源，主要用于引导页每一页都是只显示图片的情况
        // Bitmap 的宽高在 maxWidth maxHeight 和 minWidth minHeight 之间
        // BGALocalImageSize localImageSize = new BGALocalImageSize(720, 1280, 320, 640);
        // 设置数据源
//        mContentBanner.setData(localImageSize, ImageView.ScaleType.CENTER_CROP,
//                R.drawable.uoko_guide_background_1,
//                R.drawable.uoko_guide_background_2,
//                R.drawable.uoko_guide_background_3);


    }
}
