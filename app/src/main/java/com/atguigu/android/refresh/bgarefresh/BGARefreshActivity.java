package com.atguigu.android.refresh.bgarefresh;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.atguigu.android.BaseActivity;
import com.atguigu.android.R;

import cn.bingoogolapple.bgabanner.BGABanner;
import cn.bingoogolapple.refreshlayout.BGANormalRefreshViewHolder;
import cn.bingoogolapple.refreshlayout.BGARefreshLayout;

public class BGARefreshActivity extends BaseActivity implements BGARefreshLayout.BGARefreshLayoutDelegate {

    private BGARefreshLayout mRefreshLayout;
    private BGABanner mBanner;
    private TabLayout mThemeTabTool;
    private ViewPager mContentVp;

    private Fragment[] mFragments;
    private String[] mTitles;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refresh_bga);

        initView();
        initData();
    }

    @Override
    protected void initView() {
        mRefreshLayout = findViewById(R.id.refreshLayout);
        mBanner = findViewById(R.id.banner);
        mThemeTabTool = findViewById(R.id.TabLayout);
        mContentVp = findViewById(R.id.vp_viewpager_content);

        mRefreshLayout.setDelegate(this);
        mRefreshLayout.setRefreshViewHolder(new BGANormalRefreshViewHolder(mContext, true));


        mFragments = new Fragment[4];
    }

    @Override
    protected void initData() {

    }


    @Override
    public void onBGARefreshLayoutBeginRefreshing(BGARefreshLayout refreshLayout) {

    }

    @Override
    public boolean onBGARefreshLayoutBeginLoadingMore(BGARefreshLayout refreshLayout) {
        return false;
    }
}
