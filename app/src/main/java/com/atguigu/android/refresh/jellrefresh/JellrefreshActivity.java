package com.atguigu.android.refresh.jellrefresh;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.atguigu.android.BaseActivity;
import com.atguigu.android.R;

import uk.co.imallan.jellyrefresh.JellyRefreshLayout;
import uk.co.imallan.jellyrefresh.PullToRefreshLayout;

/**
 * 刷新
 */
public class JellrefreshActivity extends BaseActivity {

    private JellyRefreshLayout jellyRefreshLayout;
    private ListView mListView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refresh_jell);

        initView();
        initData();
    }

    @Override
    protected void initView() {

        jellyRefreshLayout = findViewById(R.id.JellyRefreshLayout);
        jellyRefreshLayout.setPullToRefreshListener(new PullToRefreshLayout.PullToRefreshListener() {
            @Override
            public void onRefresh(PullToRefreshLayout pullToRefreshLayout) {
                pullToRefreshLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        jellyRefreshLayout.setRefreshing(false);
                    }
                }, 3000);
            }
        });

        View loadingView = LayoutInflater.from(this).inflate(R.layout.view_loading, null);
        jellyRefreshLayout.setLoadingView(loadingView);
    }

    @Override
    protected void initData() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_refresh) {
//            jellyRefreshLayout.post(new Runnable() {
//                @Override
//                public void run() {
//                    jellyRefreshLayout.setRefreshing(true);
//                }
//            });
//            jellyRefreshLayout.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    jellyRefreshLayout.setRefreshing(false);
//                }
//            }, 3000);
//            return true;
//        }
        return super.onOptionsItemSelected(item);
    }
}
