package com.atguigu.android.dialog;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.atguigu.android.BaseActivity;

public class SuspensionWinActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initView();
        initData();
    }

    @Override
    protected void initView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SOSAlertDialog sosAlertDialog = new SOSAlertDialog(mContext);
            }
        }, 5000);    //延时1s执行
    }

    @Override
    protected void initData() {

    }
}
