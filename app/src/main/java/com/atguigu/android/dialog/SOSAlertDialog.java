package com.atguigu.android.dialog;

import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import com.atguigu.android.R;

/**
 * sos 对话框
 */
public class SOSAlertDialog {
    private static final String TAG = SOSAlertDialog.class.getSimpleName();

    private Context mContext;
    private static WindowManager mWindowManager;
    private static View rootView;

    public static Boolean isShown = false;

    public SOSAlertDialog(@NonNull Context context) {
        mContext = context.getApplicationContext();
        init();
    }

    private void init() {
        // 获取WindowManager
        mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);

        // 设置参数
        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        // 设置flag
        int flags = WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;
        params.flags = flags;
        // 不设置这个弹出框的透明遮罩显示为黑色
        params.format = PixelFormat.TRANSLUCENT;
        params.width = WindowManager.LayoutParams.WRAP_CONTENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.CENTER;

        // mWindow.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            params.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            params.type = WindowManager.LayoutParams.TYPE_PHONE;
        }

        // 加载布局
        rootView = LayoutInflater.from(mContext).inflate(R.layout.dialog_sos_alert, null);
        rootView.findViewById(R.id.btn_yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWindowManager.removeView(rootView);
            }
        });

        mWindowManager.addView(rootView, params);
    }

    /**
     * 隐藏弹出框
     */
    public static void hidePopupWindow() {
        Log.i(TAG, "hide " + isShown);
        mWindowManager.removeView(rootView);
    }
}
