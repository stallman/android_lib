package com.atguigu.android.nestedscrollview;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.atguigu.android.R;

public class NestedScrollViewActivity extends AppCompatActivity {

    private Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nestedscroll_view);
        mContext = this;

        initView();
        initData();
    }

    private void initView() {

    }

    private void initData() {

    }


}
