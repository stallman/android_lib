package com.atguigu.android.load;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.atguigu.android.R;

public abstract class LoadingFrame extends FrameLayout {

    private Context mContext;

    // 状态枚举
    public enum PageState {
        STATE_LOADING(1), /*加载中*/
        STATE_LOADERROR(2), /*加载失败*/
        STATE_NETERROR(3), /*网络错误*/
        STATE_LOADED(4), /*加载完成*/
        STATE_NODATA(5); /*无数据可显示*/
        private int value;

        PageState(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    //当前的状态值，用于记录当前网络请求的状态
    private PageState currentState = PageState.STATE_LOADING;

    // 正在加载布局
    private LinearLayout mlinearLayoutLoading;
    // 加载错误布局
    private LinearLayout mlinearLayoutLoadError;
    // 网络错误布局
    private LinearLayout mlinearLayoutNetError;
    // 无数据布局
    private LinearLayout mlinearLayoutNoData;

    // 正在加载image
    private ImageView loadingView;
    // 网络错误image
    private ImageView netErrorView;
    // 无数据image
    private ImageView noDataView;
    // 成功加载的view
    private View successView;

    public LoadingFrame(@NonNull Context context) {
        super(context);
        mContext = context;
        init();
    }

    public LoadingFrame(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public LoadingFrame(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public LoadingFrame(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
        init();
    }


    private LayoutParams params;

    private void init() {
        params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;

        createLoadingView();
        createLoadedErrorView();
        createNetErrorView();
        createNoDataView();

        addView(mlinearLayoutLoading, params);
        addView(mlinearLayoutLoadError, params);
        addView(mlinearLayoutNetError, params);
        addView(mlinearLayoutNoData, params);

        refreshView();
    }

    /**
     * 创建正在加载布局
     */
    private void createLoadingView() {
        mlinearLayoutLoading = new LinearLayout(mContext);
        LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        linearLayoutParams.gravity = Gravity.CENTER;
        mlinearLayoutLoading.setOrientation(LinearLayout.VERTICAL);

        loadingView = new ImageView(mContext);
        loadingView.setImageResource(R.drawable.common_loading_animation);
        AnimationDrawable animationDrawable = (AnimationDrawable) loadingView.getDrawable();
        animationDrawable.start();
        mlinearLayoutLoading.addView(loadingView, linearLayoutParams);
        TextView textView = new TextView(mContext);
        textView.setText("正在加载中");
        mlinearLayoutLoading.addView(textView, linearLayoutParams);
        mlinearLayoutLoading.setVisibility(View.GONE);
    }

    /**
     * 加载错误布局
     */
    private void createLoadedErrorView() {
        mlinearLayoutLoadError = new LinearLayout(mContext);
        LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        linearLayoutParams.gravity = Gravity.CENTER;
        mlinearLayoutLoadError.setOrientation(LinearLayout.VERTICAL);

        noDataView = new ImageView(mContext);
        noDataView.setImageResource(R.drawable.ic_launcher); // nodata
        mlinearLayoutLoadError.addView(noDataView, linearLayoutParams);
        TextView textView = new TextView(mContext);
        textView.setText("加载失败！点击重试");
        textView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //点击操作的响应
                currentState = PageState.STATE_LOADING;
                show();
                refreshView();
            }
        });
        mlinearLayoutLoadError.addView(textView, linearLayoutParams);
        mlinearLayoutLoadError.setVisibility(View.GONE);
    }

    /**
     * 网络错误布局
     */
    private void createNetErrorView() {
        mlinearLayoutNetError = new LinearLayout(mContext);
        LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        linearLayoutParams.gravity = Gravity.CENTER;
        mlinearLayoutNetError.setOrientation(LinearLayout.VERTICAL);

        netErrorView = new ImageView(mContext);
        netErrorView.setImageResource(R.drawable.ic_launcher); //net_error

        mlinearLayoutNetError.addView(netErrorView, linearLayoutParams);
        TextView textView = new TextView(mContext);
        textView.setText("网络错误，检查您的网络或点击重试");
        textView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //点击操作的响应

            }
        });

        mlinearLayoutNetError.addView(textView, linearLayoutParams);
        mlinearLayoutNetError.setVisibility(View.GONE);
    }

    /**
     * 无数据布局
     */
    private void createNoDataView() {
        mlinearLayoutNoData = new LinearLayout(mContext);
        LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        linearLayoutParams.gravity = Gravity.CENTER;
        mlinearLayoutNoData.setOrientation(LinearLayout.VERTICAL);

        noDataView = new ImageView(mContext);
        noDataView.setImageResource(R.drawable.ic_launcher); // nodata
        mlinearLayoutNoData.addView(noDataView, linearLayoutParams);
        TextView textView = new TextView(mContext);
        textView.setText("没有数据可供显示！");
        mlinearLayoutNoData.addView(textView, linearLayoutParams);
        mlinearLayoutNoData.setVisibility(View.GONE);
    }

    private void refreshView() {
        mlinearLayoutLoading.setVisibility(currentState == PageState.STATE_LOADING ? View.VISIBLE : View.GONE);
        mlinearLayoutNoData.setVisibility(currentState == PageState.STATE_NODATA ? View.VISIBLE : View.GONE);
        mlinearLayoutNetError.setVisibility(currentState == PageState.STATE_NETERROR ? View.VISIBLE : View.GONE);
        mlinearLayoutLoadError.setVisibility(currentState == PageState.STATE_LOADERROR ? View.VISIBLE : View.GONE);
        if (successView != null) {
            successView.setVisibility(currentState == PageState.STATE_LOADED ? View.VISIBLE : View.GONE);
        }
    }


    private void show() {
        initData();
    }

    private void initData() {
        currentState = PageState.STATE_LOADING;
        int code = onLoad();
        if (code == 200) {
            ((Activity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    currentState = PageState.STATE_LOADED;
                    successView = onSuccessView();
                    addView(successView, params);
                }
            });
        }
        refreshView();
    }


    /**
     * 成功
     */
    public abstract View onSuccessView();

    /**
     * 载入数据
     */
    public abstract int onLoad();

}